#!/usr/bin/env bash

docker run -it \
    --rm \
    -v $(pwd):/project \
    -w /project \
    php:7.2-cli \
    php -f index.php
