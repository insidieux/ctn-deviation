# Container deviation

## Composer install
```bash
docker run -it \
    --rm \
    -v $(pwd):/app \
    composer install
```

## Run
```bash
./run.sh
```
