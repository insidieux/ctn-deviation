<?php
require_once __DIR__ . '/vendor/autoload.php';

use Phpml\Math\Statistic\StandardDeviation;

$json = \file_get_contents('./sets.json');
$json = \trim($json);
$data = \json_decode($json, true);


$results = [];
foreach ($data['data']['result'] as $item) {
    if (empty($item['metric']['name'])) {
        continue;
    }
    if (!isset($results[$item['metric']['name']])) {
        $results[$item['metric']['name']] = 0.00;
    }
    $population = \array_map(function ($item) {
        return (\floatval($item));
    }, \array_column($item['values'], 1));
    try {
        $results[$item['metric']['name']] = StandardDeviation::population($population);
    } catch (\Exception $e) {}
}

\arsort ($results);
foreach (\array_slice($results, 0, 10) as $name => $value) {
    echo \sprintf("Container %s\t%s\n", $name, $value);
}
